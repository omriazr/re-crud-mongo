import log from "@ajar/marker";
import fs from "fs";
import { NextFunction, Request, Response } from "express";
import HttpException from "../exceptions/Http.exception.js";
import { ErrorResponse } from "../types/types.js";
import UrlNotFoundException from "../exceptions/UrlNotFound.exception.js";

const { White, Reset, Red } = log.constants;
const { NODE_ENV } = process.env;

export function printErrorMiddleware(
    error: HttpException,
    req: Request,
    res: Response,
    next: NextFunction
) {
    log.red(error.status, error.stack);
    next(error);
}

export function logErrorMiddleware(path: string) {
    // https://nodejs.org/api/fs.html#file-system-flags
    // 'a': Open file for appending. The file is created if it does not exist.
    const errorsFileLogger = fs.createWriteStream(path, { flags: "a" });

    return (
        error: HttpException,
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        // new log entry ->
        errorsFileLogger.write(
            `${error.status} :: ${error.message} >> ${error.stack} \n`
        );

        next(error);
    };
}

export function respondWithErrorMiddleware(
    error: HttpException,
    req: Request,
    res: Response,
    next: NextFunction
) {
    const output: ErrorResponse = {
        status: error.status || 500,
        message: error.message || "internal server error...",
    };
    console.log(output);
    if (NODE_ENV !== "production") output.stack = error.stack;
    res.status(output.status).json(output);
}

export function urlNotFoundMiddleware(
    req: Request,
    res: Response,
    next: NextFunction
) {
    next(new UrlNotFoundException(req.url));
}
