import fs from "fs";
import { NextFunction, Request, Response } from "express";

export function updateLogFile(path: string) {
    const logs = fs.createWriteStream(path, { flags: "a+" });
    return (req: Request, res: Response, next: NextFunction): void => {
        logs.write(`${req.method} ${req.path} ${Date.now()}\n`);
        next();
    };
}
