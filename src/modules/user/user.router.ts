/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import user_model from "./user.model.js";
import express from 'express';
import { CREATE_USER, UPDATE_USER, validate_user } from './user.validate.js';
import { NextFunction, Request, Response } from 'express';
import UrlNotFoundException from "../../exceptions/UrlNotFound.exception.js";
import HttpException from "../../exceptions/Http.exception.js";



const router = express.Router();

// parse json req.body on post routes
router.use(express.json())


// CREATES A NEW USER
router.post("/", raw(async (req: Request, res: Response, next:NextFunction) => {
  // const {error, value} = await validate_user(req.body, CREATE_USER);
  const valid_user = await validate_user(req.body, CREATE_USER);
  const user = await user_model.create(valid_user);
  res.status(200).json(user);
}));



// GET ALL USERS
router.get("/", raw(async (req: Request, res: Response, next:NextFunction) => {
  const users = await user_model.find()
    .select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
  if (!users) return next(new HttpException(404, `No users found!`));
  res.status(200).json(users);
})
);

router.get("/paginate/:page/:count", raw(async (req: Request, res: Response) => {
  const users = await user_model.find()
    .select(`-_id 
                                        first_name 
                                        last_name 
                                        email 
                                        phone`).skip(Number(req.params.page) * Number(req.params.count)).limit(Number(req.params.count));
  res.status(200).json(users);
})
);

// GETS A SINGLE USER
router.get("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
  const user = await user_model.findById(req.params.id)
  if (!user) return next(new HttpException(404, "No user found." ));
  res.status(200).json(user);
})
);
// UPDATES A SINGLE USER
router.put("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
  const valid_user = await validate_user(req.body, UPDATE_USER);
  const user = await user_model.findByIdAndUpdate(req.params.id, valid_user,
    { new: true, upsert: false });
  if (!user) return next(new HttpException(404, `id: ${req.params.id} not found` ));
  res.status(200).json(user);
})
);


// DELETES A USER
router.delete("/:id", raw(async (req: Request, res: Response, next: NextFunction) => {
  const user = await user_model.findByIdAndRemove(req.params.id);
  console.log(user);
  if (!user) {
    return next(new UrlNotFoundException(req.originalUrl));
  }
  res.status(200).json(user);
})
);

export default router;
