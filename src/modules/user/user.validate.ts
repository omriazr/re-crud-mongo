import Joi from 'joi';
import {User} from "../../types/types.js"

export const CREATE_USER = 'CREATE_USER';
export const UPDATE_USER = 'UPDATE_USER';

export const validate_user = async (payload:User, type = UPDATE_USER) => {
    let first_name = Joi.string().alphanum().min(3).max(30);
    let last_name = Joi.string().alphanum().min(3).max(30);
    let phone = Joi.string().min(9).max(14).pattern(/^[0-9-]+$/);
    let email = Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'ac', 'net'] } });

    if (type === CREATE_USER) {
        first_name = first_name.required();
        last_name = last_name.required();
        phone = phone.required();
        email = email.required();
    }

    console.log(payload);

    const schema = Joi.object().keys({ first_name, last_name, email, phone });

    return await schema.validateAsync(payload);
}