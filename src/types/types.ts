export interface User {
    first_name?: string;
    last_name?: string;
    email?: string;
    phone?: string;
}

export interface ErrorResponse {
    status: number;
    message: string;
    stack?: string;
}
