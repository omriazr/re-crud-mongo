import HttpException from "./Http.exception.js";

class UrlNotFoundException extends HttpException {
    constructor(path: string) {
        super(404, `Url with path ${path} not found`);
    }
}

export default UrlNotFoundException;
